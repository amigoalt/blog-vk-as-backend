from app import create_app
from werkzeug.debug import DebuggedApplication

application = create_app()

application.wsgi_app = DebuggedApplication(application.wsgi_app, True)
application.debug = True
