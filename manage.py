import os
from app import create_app
from app import config

app = create_app(config=config.DevelopmentConfig)
# app.config.from_object(os.environ['APP_SETTINGS'])
