import logging
import os
from flask import Flask
import flask_assets
from app import config
from app.main import main
from app.main.models import (
    database, create_tables)


# def create_app(config=config.ProductionConfig):
def create_app(config=config.DevelopmentConfig):
    app = Flask(__name__)
    app.config.from_object(config)

    # add filemode="w" to overwrite
    # filename = '/home/smevna/advokatsmirnova.ru/logs/mylog.log'
    # filename = '/home/s/smevna/advokatsmirnova.ru/logs/mylog.log'
    filename = app.config.get("LOGFILE")
    logging.basicConfig(filename=filename, level=logging.DEBUG)
    logging.debug("my INFO: %s" % app.config.get("LOGFILE"))

    register_extensions(app)
    register_blueprints(app)
    register_assets(app, config)

    create_tables()

    @app.before_request
    def _db_connect():
        database.connect()
        # database.pragma('foreign_keys', 1, permanent=True)

    # This hook ensures that the connection is closed when we've finished
    # processing the request.
    @app.teardown_request
    def _db_close(exc):
        if not database.is_closed():
            database.close()

    return app


def register_extensions(app):
    """Register extensions with the Flask application."""


def register_blueprints(app):
    """Register blueprints with the Flask application."""
    app.register_blueprint(main)


def register_assets(app, config):
    assets = flask_assets.Environment()
    assets.init_app(app)

    with app.app_context():
        assets.load_path = [
            os.path.join(os.path.dirname(__file__), 'main/sass'),
            os.path.join(os.path.dirname(__file__), 'main/coffee'),
            os.path.join(os.path.dirname(__file__), 'static/js'),
            os.path.join(os.path.dirname(__file__), '../bower_components')]

        coffee = flask_assets.Bundle(
            'bus.coffee',
            'home.coffee',
            'popups.coffee',
            filters=['coffeescript'])

        js = flask_assets.Bundle(
            'menu.js',
            coffee,
            output='js/js_all.js')

        css = flask_assets.Bundle(
            'custom_bulma.sass',
            '_menu_mobile.sass',
            filters=['libsass'],
            output='css/css_all.css')

        assets.register('js_all', js)
        assets.register('css_all', css)
