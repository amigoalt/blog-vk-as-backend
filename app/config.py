import os


class BaseConf(object):
    DEBUG = True
    CSRF_ENABLED = True
    SECRET_KEY = '_5#y2L"F4Q8z\n\xec]/'
    MAX_CONTENT_LENGTH = 500000
    VK_ACCESS_TOKEN = '072e53fea14a16854f9203c5d39bec26ea10d13f6cf931d991f93a4e1088819344d8d1dc4aa0a500c4049'
    JSON_AS_ASCII = False


class ProductionConfig(BaseConf):
    DEBUG = False
    APP_PATH = '/home/smevna/advokatsmirnova.ru/app'
    PROJECT_PATH = '/home/smevna/advokatsmirnova.ru'
    PANDOC_PATH = '/usr/bin/pandoc'
    UPLOAD_FOLDER = APP_PATH + '/static/files/'
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    # MAIL_USERNAME = os.environ['EMAIL_USER'],
    # MAIL_PASSWORD = os.environ['EMAIL_PASSWORD']
    MAIL_USERNAME = 'sctadiesel@gmail.com'
    MAIL_PASSWORD = 'koksha650'
    LOGFILE = '/home/smevna/logs/Production.log'



class DevelopmentConfig(BaseConf):
    DEBUG = True
    APP_PATH = '/home/good/python_projects/blog-vk-as-backend/app'
    PROJECT_PATH = '/home/good/python_projects/blog-vk-as-backend'
    PANDOC_PATH = '/home/good/anaconda3/bin/pandoc'
    UPLOAD_FOLDER = APP_PATH + '/static/files/'
    LOGFILE = PROJECT_PATH + '/logs/Development.log'
