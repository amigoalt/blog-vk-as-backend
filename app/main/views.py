import re
import urllib
import logging
# from app import config
import time
import peewee as pw
from flask import (
    render_template,
    make_response,
    json,
    send_from_directory,
    send_file,
    current_app,
    request,
    Response,
    session,
    jsonify
)
from ..main import main
from .models import (
    Entry, Tag, EntryTag, Menu, MenuTag,
    Provider, Vkontakte, Twitter,
    Pagination
)
import requests
from app.main.comm_runner import comm_runner

import wikivk.core as wiki

import vk_api
login, password = 'amigoalt@gmail.com', 'aA3845915009'
vk_session = vk_api.VkApi(login, password)
vk = vk_session.get_api()

try:
    vk_session.auth(token_only=True)
except vk_api.AuthError as error_msg:
    print(error_msg)


@main.route('/', methods=['GET'])
def index():
    return render_template('home.html')


@main.route('/favicon.ico')
def favicon():
    return send_file('favicon.ico')


@main.route('/about', methods=['GET'])
def about():
    return render_template('about.html')


@main.route('/contacts', methods=['GET'])
def contacts():
    return render_template('contacts.html')


@main.route('/blog/<int:id>', methods=['GET'])
def blog_entry(id):
    entry = (Entry().select(Entry).where(
        Entry.id == id
    ).get())
    return render_template('blog_entry.html', entry=entry)


@main.route('/blog/', defaults={'page': 1})
@main.route('/blog/search/<filter>', defaults={'page': 1})
@main.route('/blog/page/<int:page>')
@main.route('/blog/search/<filter>/page/<int:page>')
def blog(page=1, filter=''):
    filter = urllib.parse.unquote(filter)
    entries_per_page = 15
    if len(filter) > 0:
        decoded = urllib.parse.unquote(filter)
        '''
        Comment the following stroke because it case sensitive
        entries = Product.select().where(Product.name.contains(filter))
            .paginate(
                page, entries_per_page
            )
        add lowercase_search() to make search case insensitive.
        See @database.func('lowercase_search') in model.py
        '''
        entries = Entry().select().where(
            pw.fn.synonyms_search(Entry.title, decoded)).paginate(
                page, entries_per_page)
        total_entries = Entry.select().where(
            pw.fn.synonyms_search(Entry.title, decoded)).count()
    else:
        entries = Entry.select().paginate(page, entries_per_page)
        total_entries = Entry.select().count()

    pagination = Pagination(page, entries_per_page, total_entries)
    filter = Entry.convert_bad_synonym_to_good_phrase(filter)

    return render_template(
        'blog.html',
        pagination=pagination,
        entries=entries,
        page=page,
        filter=filter)


@main.route('/api/search/<phrase>')
def search_result(phrase):
    decoded = urllib.parse.unquote(phrase)
    query = Entry().select().where(pw.fn.synonyms_search(
        Entry.name, decoded))
    output = {}
    output['results'] = []

    for result in query:
        output['results'].append({'name': result.name})

    return json.dumps(output, ensure_ascii=False)


@main.route('/api/vk-callback', methods=["POST", "GET"])
def vk_feed():
    if request.method == 'POST':
        logging.info('==== POST FROM VK ===')
        logging.info(request.get_json())
        data_type = request.get_json().get('type')
        group_id = request.get_json().get('group_id')

        # log = request.get_json()
        # with open("test0.txt", "wb") as fo:
        #    fo.write(log.encode())

        if 'confirmation' == data_type and group_id == 179435527:
            # Для Confirmation API
            return make_response('98e37a71')
        elif 'wall_post_new' == data_type and group_id == 179435527:
            # Если добавлен новый пост ВКонтакте
            post = request.get_json().get('object')
            wiki_page_id = post['attachments'][0]['page']['id']

            # Получить Wiki-страницу из ВКонтакте в БД,
            # используя https://vk.com/dev/pages.get
            page = vk.pages.get(
                page_id=wiki_page_id,
                group_id=group_id,
                need_html=True,
                need_source=True
            )
            # Поместить содержимое Wiki-страницы в БД
            pid = Provider.create_or_update("vkontakte")
            Entry.create_or_update(page, pid)
        elif 'wall_reply_new' == data_type and group_id == 179435527:
            '''
            TODO паресер команд, поступающих из ВКонтакте
            '''
            # Берём post_text
            # разбираем его на команды и парамтеры команд
            # Для этого обходим в цикле справочник команд
            # и вызываем исполнителя команды

            # Если добавлен новый пост ВКонтакте
            post = request.get_json().get('object')
            post_text = post['text']
            post_id = post["post_id"]

            # Найти сходство в списке допустимых команд
            # и передать команду на выполнение в модель (Entry, Tag, Menu)
            for comm in comm_runner:
                match = re.search(comm["pattern"], post_text)
                if match:
                    status = comm["run"](commdata=match.groupdict(), post_id=54705133)
                    break

            # TODO продумать статусы
            # Сделать удаление комментария (сейчас удаляется пост)
            # Перед удалением комментария желательно написать ответ
            # а затем, через несколько секунд удалить комменты
            if "created" == status:
                vk.wall.delete(
                    owner_id="-179435527",
                    post_id=post_id
                )

        return make_response('ok')  # Требование VK API

    elif request.method == 'GET':
        # Для тестирования
        # Получить Wiki-страницу из ВКонтакте в БД,
        # используя https://vk.com/dev/pages.get
        page = vk.pages.get(
            page_id=54716962,
            group_id=179435527,
            need_html=True,
            need_source=True
        )

        pid = Provider.create_or_update("vkontakte")
        Entry.create_or_update(page, pid)

        log = "GET from BROWSER: " + " / " + " / "
        # log = request.get_json()
        with open("test.txt", "wb") as fo:
            fo.write(log.encode())

        # entry.remove(54698064)
        # provider.remove(1)
        '''
        post_text = 'new tag alibaba'

        for comm in comm_runner:
            match = re.search(comm["pattern"], post_text)
            if match:
                comm["run"](commdata=match.groupdict(), post_id=54705133)
                return make_response("FOUND: {} --- <pre>{}</pre>".format(post_text, comm["pattern"]))
        '''

    '''
    jsonified_page = jsonify(page)
    provider = Vkontakte()
    entry = vk_provider.parse(jsonified_page['view_url'])
    vk_provider.create_or_update(html)
    '''
    return jsonify(page)


@main.route('/api/sse')
def stream():
    event_name = 'vk-message'

    def event_stream(event_name='vk-message'):
        new_post = 'none 22'
        new_post = wiki.convert_html("'''sdsdssd'''")

        # q = Provider.delete().where(True)
        # q.execute()  # Remove the rows, return number of rows removed.
        yield 'event: %s\ndata: %s\n\n' % (event_name, new_post)

    return Response(event_stream(event_name), mimetype="text/event-stream")


'''
@main.route('/api/sse')
def mstream():
    event_name = 'vk-message'
    # event_name = 'message'
    def event_stream(event_name='message'):
        if 'wall_post_new' in session:
            new_post = session['wall_post_new']
            session.clear()
            yield 'event: %s\ndata: post = %s\n\n' % (event_name, new_post)
        else:
            yield 'event: %s\ndata: no posts\n\n' % (event_name)

    return Response(event_stream(event_name), mimetype="text/event-stream")
    # return make_response('98e37a71')
'''


@main.route('/api/parse-vk-wiki', methods=['GET'])
def parse_vk_wiki():
    '''
    url = 'https://vk.com/advokatsmirnovaru?w=page-179435527_54698083'
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    # text_body = soup.find(id='wk_content') #soup.find("div", id="wk_content")
    text_body = soup.find_all("div", class_='wiki_body')
    return render_template('crawler.html', text_body=text_body)
    '''
    session['wall_post_new'] = 'my new post'
    return make_response(session['wall_post_new'])


@main.app_errorhandler(404)
def handle_404(err):
    return render_template('404.html'), 404


'''
@main.app_errorhandler(500)
def handle_500(err):
    return render_template('500.html'), 500
'''
