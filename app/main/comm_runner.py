
from .models import (
    Entry, Tag, EntryTag, Menu, MenuTag,
    Provider, Vkontakte, Twitter,
    Pagination
)

comm_runner = [
    # Marking blog entries with tag
    {"pattern": '^new tag (?P<tagname>\w{2,10})$',
        "run": Tag.create_or_update},
    {"pattern": '^tag (?P<tagname>\w{2,10})$', "run": Entry.mark_with_tag},
    {"pattern": '^tag (?P<oldname>\w{2,10}) rename (?P<newname>\w{2,10})$', 
        "run": Tag.rename},
    {"pattern": '^tag (?P<tagname>\w{2,10}) remove$', "run": Tag.remove},
    {"pattern": '^tag (?P<tagname>\w{2,10}) erase$', "run": Tag.erase},

    # Change blog entries order
    {"pattern": '^order (?P<order>first)$', "run": Entry.make_order},
    {"pattern": '^order (?P<order>last)$', "run": Entry.make_order},
    {"pattern": '^order (?P<order>[1-9])$', "run": Entry.make_order},

    # Web-site menu management
    {"pattern": '^new menu (?P<menuname>\w{3,20}) tag (?P<tagname>\w{2,10})$', 
        "run": Menu.create_or_update},
    {"pattern": '^menu (?P<oldmenuname>\w{3,20}) update (?P<newmenuname>\w{3,20})$',
        "run": Menu.create_or_update},
    {"pattern": '^menu (?P<menuname>\w{3,20}) erase$', "run": Menu.erase},
    {"pattern": '^menu (?P<menuname>\w{3,20}) add tag (?P<tagname>\w{2,10})$',
        "run": Menu.add_tag},
    {"pattern": '^menu (?P<menuname>\w{3,20}) remove tag (?P<tagname>\w{2,10})$',
        "run": Menu.remove_tag},
    {"pattern": '^menu order (?P<order>[1-9])$',
        "run": Menu.make_order}
]
