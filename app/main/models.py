import logging
import peewee as pw
from app.database import database
import datetime
from math import ceil
from flask import Markup
# import pypandoc
import wikivk.core as wiki

import os
# os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')
os.environ.setdefault('PYPANDOC_PANDOC', '/home/good/anaconda3/bin/pandoc')
# os.environ.setdefault('PYPANDOC_PANDOC', app.config.get('PANDOC_PATH'))


@database.func('lowercase_search')
def lowercase_search(name, filter):
    return filter.lower() in name.lower()


@database.func('synonyms_search')
def synonyms_search(target, filter):
    return Entry.is_in_synonyms(target, filter)


def create_tables():
    pass
    '''
    with database:
        database.drop_tables([Provider, Entry, Tag, Menu, MenuTag, EntryTag])
        database.create_tables([
            Provider, Entry, Tag, Menu, MenuTag, EntryTag
        ])
    '''


class BaseModel(pw.Model):
    class Meta:
        database = database


class Provider(BaseModel):
    id = pw.AutoField(null=False)
    name = pw.CharField(unique=True)

    class Meta:
        table_name = "providers"

    @staticmethod
    def create_or_update(provider_name):
        logging.info("===PROVIDER=== %s", provider_name)
        # Создать запись в таблице, если ещё нет с таким name
        provider, created = Provider.get_or_create(
            name=provider_name,
            defaults={
                'name': provider_name
            }
        )
        if created is False:
            # Если запись уже есть
            # то обновляем её
            setattr(provider, 'name', provider_name)
            provider.save()
        return provider.get_id()

    @staticmethod
    def remove(id=None):
        Provider.delete().where(Provider.id == id).execute()


class Vkontakte(Provider):
    def parse(url=''):
        ...


class Twitter(Provider):
    def parse(url=''):
        ...


class Entry(BaseModel):
    id = pw.AutoField(null=False)
    title = pw.CharField()
    content = pw.CharField()
    timestamp = pw.DateTimeField(default=datetime.datetime.now())
    provider_id = pw.ForeignKeyField(
        Provider,
        backref="entries",
        field="id",
        on_delete="cascade",
        on_update="cascade"
    )
    provided_content_id = pw.CharField()
    order = pw.IntegerField(null=True)

    synonyms = {"алименты": (
        "элементы",
        "элименты",
        "алементы",
        "kbvtyns",
        "ktvtyns",
        "kbvtyns",
        "ktvtyn"),
        "иск": ("bcr", "isk"),
        "претензия": ("притензия", "ghtntypbz", "pretenziya")
    }

    class Meta:
        table_name = "entries"

    @staticmethod
    def create_or_update(page, provider_id):
        # Создать запись в таблице, если ещё нет
        # с такими provided_content_id и provider
        entry, created = Entry.get_or_create(
            provided_content_id=page['id'],
            provider_id=provider_id,
            defaults={
                'title': page['title'],
                'content': page['source'],
                'provided_content_id': page['id']
            }
        )
        if created is False:
            # Если запись уже есть
            # то обновляем её
            setattr(entry, 'title', page['title'])
            setattr(entry, 'content', page['source'])
            setattr(entry, 'timestamp', page['edited'])
            entry.save()
        return entry.get_id()

    @staticmethod
    def remove(id=None):
        Entry.delete().where(Entry.provided_content_id == id).execute()

    @staticmethod
    def convert_bad_synonym_to_good_phrase(phrase=''):
        '''
        Это нужно, чтобы после неудачного ввода, напр. "'элементы'",
        пользователь увидел на странице "Поиск по запросу алименты"
        '''
        synonyms = Entry.synonyms
        for s_key in synonyms.keys():
            if (phrase in synonyms[s_key] or phrase == s_key):
                phrase = s_key
                break
        return phrase

    @staticmethod
    def is_in_synonyms(target, filter):
        '''
        Решает проблему с русскими буквами (заглавными, строчными)
        при поиске в SQlite, не поддерживающим COLLOCATION NOCASE
        А также делает поиск по синонимам, т.е. обрабатывает опечатки
        при вводе в графе поиска.
        Функция вызывается в запросе к БД через Peewee ORM
        '''
        filter = filter.lower()
        target = target.lower()
        synonyms = Entry.synonyms
        for s_key in synonyms.keys():
            if (filter in synonyms[s_key] or filter == s_key):
                filter = s_key
                break

        return filter in target

    @property
    def html_content(self):

        ''' Let make Pandoc disable
        because Lark is used instead!

        output = pypandoc.convert(
            self.content,
            'html',
            format='mediawiki',
            extra_args=['--base-header-level=2']
        )
        '''
        tmp_cont="""
        <right>'''...или как всё благополучно окончилось'''</right>\n\n<blockquote>Рыбкина О.А. (ФИО изменено) 12.03.2018 г. в 12:06 находилась в магазине “Евроспар” у прилавка в отделе “Домашняя кухня”, когда увидела у ранее ей знакомой Е.Е.Ю, найденный ей кошелёк, принадлежавший М.Т.В., попросила Е.Е.Ю. положить вышеуказанный кошелек в ее дамскую сумку.. Далее события разворачивались так, что в итоге Рыбкина О.А. оказалась на скамье подсудимых, а адвокату Смирновой Е.В. выпала честь защищать её право на свободу. Чем всё это окончилось можно проследить по материалу данного дела. \n</blockquote> \n\nДело № 1-208/2018 <center>ПОСТАНОВЛЕНИЕ </center>\n\nг. Йошкар-Ола 26 марта 2018 года \nЙошкар-Олинский городской суд в составе: председательствующего судьи Депрейса С.А., при секретаре Васеневой К.Б., с участием прокурора – помощника прокурора г. Йошкар-Олы Бобкина Р.С., обвиняемой Рыбкиной О.А., защитника – адвоката Смирновой Е.В., представившей удостоверение № и ордер №, потерпевшей М.Т.В., рассмотрев в закрытом судебном заседании в порядке предварительного слушания уголовное дело в отношении: \nРыбкиной Ольги Анатольевны, <данные изъяты> не судимой, \nобвиняемой в совершении преступления, предусмотренного ст. 158 ч. 2 п. «в» УК РФ, \nУСТАНОВИЛ: \n\nРыбкина О.А. органами предварительного следствия обвиняется в совершении кражи, то есть тайного хищения чужого имущества, с причинением значительного ущерба гражданину при следующих обстоятельствах. \nПыбкина О.А., ДД.ММ.ГГГГ примерно в 12 часов 06 минут совместно с ранее ей знакомыми - Е.И.А., Е.Е.Ю. и Е.Ю.Ю. находилась в магазине «Eurospar» у прилавка в отделе «Домашняя кухня», когда увидела в руках у ранее ей знакомой Е.Е.Ю., найденный ей кошелек, принадлежащий М.Т.В. При этом Е.Е.Ю. спросила Рабкина О.А., не ее ли это кошелек, увидев кошелек, у Рыбкина О.А., предположившей, что внутри него имеется ценное имущество, в это же время и в этом же месте возник корыстный преступный умысел, направленный на совершение тайного хищения чужого имущества, а именно кошелька с находящимся в нем имуществом, принадлежащих ранее незнакомой ей М.Т.В., с целью противоправных безвозмездного изъятия и обращения его в свою пользу, в качестве источника личного обогащения, с причинением значительного ущерба гражданину. \nРеализуя свой корыстный, преступный умысел, направленный на тайное хищение чужого имущества, а именно кошелька с находящимся в нем имуществом, принадлежащих М.Т.В., с причинением значительного ущерба гражданину, Рыбкина О.А., умышленно, осознавая общественную опасность своих действий, предвидя возможность и неизбежность наступления общественно-опасных последствий, и желая их наступления, с целью противоправных безвозмездного изъятия чужого имущества и обращения его в свою пользу в качестве источника личного обогащения, ДД.ММ.ГГГГ примерно в 12 часов 06 минут, находясь у прилавка в отделе «Домашняя кухня» магазина «Eurospar» №<адрес> действуя из корыстных побуждений, и воспользовавшись тем обстоятельством, что рядом, кроме ранее ей знакомой Е.Е.Ю. никого нет, и за ее преступными действиями никто не наблюдает, то есть они носят тайный характер для окружающих, попросила Е.Е.Ю. положить вышеуказанный кошелек в ее дамскую сумку, при этом, введя Е.Е.Ю. в заблуждение относительно своих преступных намерений и, не посвящая ее в них, а именно сказав ей, что это ее кошелек. Е.Е.Ю., не подозревая о преступных намерениях Рыбкиной О.А., исполнила просьбу последней, и положила найденный ей кошелек в сумку Рыбкиной О.А. \nТем самым Рыбкина О.А. умышленно тайно похитила имущество, принадлежащее М.Т.В., а именно кошелек из кожи, стоимостью 2 800 рублей, с находящимися в нем денежными средствами в сумме 14000 рублей, банковскими картами ПАО «Сбербанк России» и «Россельхозбанк», чеками с купонами, с дисконтными картами, не представляющими материальной ценности. \nТайно завладев вышеуказанным имуществом, Рыбкина О.А., убедившись, что ее действия остались незамеченными, вместе с похищенным имуществом беспрепятственно скрылась с места совершения преступления. Похищенным имуществом Рыбкина О.А. распорядилась по своему усмотрению, причинив своими умышленными корыстными преступными действиями М.Т.В. значительный материальный ущерб на общую сумму 16 800 рублей. \nВ ходе предварительного слушания потерпевшей М.Т.В. было заявлено ходатайство о прекращении уголовного дела в отношении Рыбкиной О.А., в связи с примирением сторон. Правовые последствия прекращения дела по данному основанию потерпевшей разъяснены и понятны. \nОбвиняемая Рыбкина О.А. и защитник также просили прекратить уголовное дело в связи с примирением сторон. Правовые последствия прекращения дела по данному основанию Рыбкиной О.А. разъяснены и понятны. \nПрокурор с учетом характера совершенного преступления, личности Рыбкиной О.А. не возражал прекращению уголовного дела. \nВ соответствии со ст. 25 УПК РФ, суд, на основании заявления потерпевшего вправе прекратить уголовное дело в отношении лица, обвиняемого в совершении преступления небольшой или средней тяжести, в случаях, предусмотренных ст. 76 УК РФ, если это лицо примирилось с потерпевшим и загладило причиненный ему вред. \nПотерпевшая М.Т.В. ходатайствовала о прекращении уголовного дела в отношении Рыбкиной О.А., ущерб ей возмещен в полном объеме, обвиняемой в ее адрес принесены извинения, которые приняты, каких-либо претензий к Рыбкиной О.А. не имеет. \nОбвиняемая Рыбкина О.А. вину в совершении преступления средней тяжести признала полностью и раскаивается в содеянном, принесла извинения потерпевшей, которые та приняла. \nРыбкина О.А. не судима (л.д. 179-180), <данные изъяты>. \nКроме того, судом учтено добровольное возмещение ущерба потерпевшей (л.д. 255). \nПри таких обстоятельствах суд признаёт, что между Рыбкиной О.А. и потерпевшей состоялось примирение. \nУчитывая волеизъявление потерпевшей, которая желает прекратить уголовное дело в отношении Рыбкиной О.А., принимая во внимание данные о личности Рыбкиной О.А., возмещение причинённого преступлением вреда, суд полагает возможным уголовное дело прекратить по основаниям, предусмотренным ст. 25 УПК РФ и ст. 76 УК РФ, за примирением сторон, что будет соответствовать принципу справедливости и законности, соответствовать задачам уголовного судопроизводства. Препятствий к прекращению уголовного дела не имеется. \nСудом разрешен вопрос о мере пресечения, вещественных доказательствах. \nГражданский иск по делу не заявлен. \nНа основании изложенного и руководствуясь ст. 25 и ст. 239 УПК РФ, ст. 76 УК РФ, суд \n\n<center>ПОСТАНОВИЛ: </center>\n\nОсвободить от уголовной ответственности Рыбкину Ольгу Анатольевну, обвиняемую в совершении преступления, предусмотренного ст. 158 ч. 2 п. «в» УК РФ и прекратить в отношении нее уголовное дело на основании ст. 25 УПК РФ, в связи с примирением с потерпевшей М.Т.В. \nМеру пресечения в виде подписки о невыезде и надлежащем поведении в отношении Рыбкиной О.А. после вступления настоящего постановления в законную силу отменить. \nВещественные доказательства: DVD-R диск с видеозаписью; отчет движения денежных средств по банковской карте – хранить при материалах уголовного дела, кошелек из кожи зеленого цвета – вернуть Рыбкиной О.А. \nНастоящее постановление может быть обжаловано в Верховный Суд Республики Марий Эл в течение 10 суток со дня его вынесения. \nПредседательствующий С.А. Депрейс
        """
        output = wiki.convert_html(self.content)
        # return Markup(output)
        return Markup(output)

    @staticmethod
    def make_order(commdata, post_id):
        '''
        Устанавливает позицию записи блога относительно других записей
        commdata - данные команды
        post_id - post_id Vkontakte, из которого поступила команда
        '''
        # Преобразуем входную позицию в число
        if 'first' == commdata['order']:
            n = 1
        elif 'last' == commdata['order']:
            n = Entry.select(pw.fn.MAX(Entry.order)).scalar()
        else:
            n = commdata['order']

        # Увеличить на 1 поле 'order' у всех записей, у которых order=n
        Entry.update(
            {'order': Entry.order + 1}
        ).where(
            Entry.order >= n
        ).execute()

        # Установить order данной записи равным n
        Entry.update(
            {'order': n}
        ).where(
            Entry.provided_content_id == id
        ).execute()
        return

    @staticmethod
    def mark_with_tag(entry, tagname):
        '''
        Пометить тэгом запись блога
        '''
        return


class Tag(BaseModel):
    id = pw.AutoField()
    name = pw.CharField(unique=True)

    @staticmethod
    def create_or_update(commdata, post_id):
        status = ''
        tag, created = Tag.get_or_create(
            name=commdata["tagname"],
            defaults={
                "name": commdata["tagname"]
            }
        )
        if created:
            status = 'created'
        elif tag:
            status = 'updated'

        return status

    @staticmethod
    def rename(commdata, post_id):
        Tag.update(
            {'name': commdata["newname"]}
        ).where(
            Tag.name == commdata["oldname"]
        ).execute()
        return

    @staticmethod
    def remove(tagname):
        Tag.remove().where(Tag.name == tagname)
        return

    @staticmethod
    def erase(tagname):
        Tag.remove().where(Tag.name == tagname)
        return

    class Meta:
        table_name = "tags"


class EntryTag(BaseModel):
    entry_id = pw.ForeignKeyField(
        Entry,
        backref="entries",
        field="id",
        on_delete="cascade",
        on_update="cascade"
    )
    tag_id = pw.ForeignKeyField(
        Tag,
        backref="tags",
        field="id",
        on_delete="cascade",
        on_update="cascade"
    )

    class Meta:
        table_name = "entries_tags"


class Menu(BaseModel):
    id = pw.AutoField()
    title = pw.CharField(unique=True)

    class Meta:
        table_name = "menus"

    @staticmethod
    def create_or_update(commdata, post_id):
        pass

    @staticmethod
    def erase(commdata, post_id):
        pass

    @staticmethod
    def add_tag(commdata, post_id):
        pass

    @staticmethod
    def remove_tag(commdata, post_id):
        pass

    @staticmethod
    def make_order(commdata, post_id):
        pass


class MenuTag(BaseModel):
    menu_id = pw.ForeignKeyField(
        Menu,
        backref="menus",
        field="id",
        on_delete="cascade",
        on_update="cascade"
    )
    tag_id = pw.ForeignKeyField(
        Tag,
        backref="tags",
        field="id",
        on_delete="cascade",
        on_update="cascade"
    )


class Pagination(object):
    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=2, right_edge=2):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or (num > self.page - left_current - 1 and
               num < self.page + right_current) or \
               num > self.pages - right_edge:
                    if last + 1 != num:
                        yield None
                    yield num
                    last = num
