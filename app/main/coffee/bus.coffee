# General bus
eventBus = new Vue({
    methods: {
        add_to_basket: (id) ->
            @update_session_basket(id, 'add')
            return
        remove_from_basket: (id) ->
            @update_session_basket(id, 'remove')
            return
        reset_basket: ->
            @update_session_basket(0, 'reset')
            return

        update_session_basket: (id, command) =>
            axios.post('/api/session', { id: id, 'command': command }
            ).then((data) ->
                eventBus.$emit 'update_ordered_list', data.data.result
                return
            ).catch((error) ->
                throw error
                return
            ).finally =>
                @isFetching = false
                return
            return
    }
    created: () ->
        try
            ordlist = document.currentScript.getAttribute('ordered_list')
            @ordered_list = JSON.parse(ordlist)
        catch
            @ordered_list = []
            
        this.$on "add_to_basket", (id) ->
            @add_to_basket(id)
            return

        this.$on "remove_from_basket", (id) ->
            @remove_from_basket(id)
            return

        this.$on "reset_basket", () ->
            @reset_basket()
            return
})