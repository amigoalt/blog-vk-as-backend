SweetAlert = {
    methods: {
        alert: (options) ->
            swal(options)
            return

        alertSuccess: () ->
            options = {
                title: "Success!"
                text: "That's all done!"
                timer: 1000
                showConfirmationButton: false,
                type: 'success'
            }
            @alert(options)
            return
        alertError: () ->
            options = {
                title: "Error!"
                text: "Oops...Something went wrong"
                type: 'error'
            }
            @alert(options)
            return

        confirm: (callback, options) ->
            options = Object.assign({
                title: "Are you sure?",
                text: "Are you sure you want to do that?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, options)
            
            swal(options, callback)
            return
    }
}

new Vue({
    el: "#app"
    mixins: [SweetAlert]
    methods: {
        doSuccess: () ->
            @alertSuccess()
            return

        doError: () ->
            @alertError()
            return

        doConfirm: () ->
            callback = {
                successFunc: () =>
                    @alertSuccess({ title: "Confirm Succcessful!" })
                    return
            }
            @confirm(callback["successFunc"])
            return
    }
})