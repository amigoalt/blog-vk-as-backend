document.addEventListener("DOMContentLoaded", () => {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll(".navbar-burger"), 0);
    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
    
        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener("click", () => {
    
                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);
    
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle("is-active");
                $target.classList.toggle("is-active");
            });
        });
    }
});
// General bus
var SweetAlert, eventBus;

eventBus = new Vue({
  methods: {
    add_to_basket: function(id) {
      this.update_session_basket(id, 'add');
    },
    remove_from_basket: function(id) {
      this.update_session_basket(id, 'remove');
    },
    reset_basket: function() {
      this.update_session_basket(0, 'reset');
    },
    update_session_basket: (id, command) => {
      axios.post('/api/session', {
        id: id,
        'command': command
      }).then(function(data) {
        eventBus.$emit('update_ordered_list', data.data.result);
      }).catch(function(error) {
        throw error;
      }).finally(() => {
        this.isFetching = false;
      });
    }
  },
  created: function() {
    var ordlist;
    try {
      ordlist = document.currentScript.getAttribute('ordered_list');
      this.ordered_list = JSON.parse(ordlist);
    } catch (error1) {
      this.ordered_list = [];
    }
    this.$on("add_to_basket", function(id) {
      this.add_to_basket(id);
    });
    this.$on("remove_from_basket", function(id) {
      this.remove_from_basket(id);
    });
    return this.$on("reset_basket", function() {
      this.reset_basket();
    });
  }
});

Vue.component('sse', {
  data: function() {
    return {
      stockData: '555'
    };
  },
  created: function() {
    this.setupStream();
  },
  methods: {
    setupStream: function() {
      var es;
      console.log('setypStream tun');
      es = new EventSource('/api/sse');
      es.addEventListener('vk-message', this.fireEvent, false);
    },
    fireEvent: function(e) {
      var data;
      // data = JSON.parse(event.data)
      data = event.data;
      console.log('sdsd ', data);
      this.stockData += data;
    }
  }
});

new Vue({
  el: '#sse_id',
  delimiters: ['[[', ']]']
});

SweetAlert = {
  methods: {
    alert: function(options) {
      swal(options);
    },
    alertSuccess: function() {
      var options;
      options = {
        title: "Success!",
        text: "That's all done!",
        timer: 1000,
        showConfirmationButton: false,
        type: 'success'
      };
      this.alert(options);
    },
    alertError: function() {
      var options;
      options = {
        title: "Error!",
        text: "Oops...Something went wrong",
        type: 'error'
      };
      this.alert(options);
    },
    confirm: function(callback, options) {
      options = Object.assign({
        title: "Are you sure?",
        text: "Are you sure you want to do that?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      }, options);
      swal(options, callback);
    }
  }
};

new Vue({
  el: "#app",
  mixins: [SweetAlert],
  methods: {
    doSuccess: function() {
      this.alertSuccess();
    },
    doError: function() {
      this.alertError();
    },
    doConfirm: function() {
      var callback;
      callback = {
        successFunc: () => {
          this.alertSuccess({
            title: "Confirm Succcessful!"
          });
        }
      };
      this.confirm(callback["successFunc"]);
    }
  }
});
