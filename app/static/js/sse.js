let msgServer;

export default {
  name: 'sse-test',
  data() {
    return {
      messages: [],
    };
  },
  mounted() {
    (async () => {
      try {
        // Store SSE object at a higher scope
        msgServer = await $sse('/your-events-server', { format: 'json' }); // omit for no format pre-processing

        // Catch any errors (ie. lost connections, etc.)
        msgServer.onError(e => {
          console.error('lost connection; giving up!', e);

          // This is purely for example; EventSource will automatically
          // attempt to reconnect indefinitely, with no action needed
          // on your part to resubscribe to events once (if) reconnected
          msgServer.close();
        });

        // Listen for messages without a specified event
        msgServer.subscribe('', data => {
          console.warn('Received a message w/o an event!', data);
        });

        // Listen for messages based on their event (in this case, "chat")
        msgServer.subscribe('chat', message => {
          this.messages.push(message);
        });

        // Unsubscribes from event-less messages after 7 seconds
        setTimeout(() => {
          msgServer.unsubscribe('');

          console.log('Stopped listening to event-less messages!');
        }, 7000);

        // Unsubscribes from chat messages after 7 seconds
        setTimeout(() => {
          msgServer.unsubscribe('chat');

          console.log('Stopped listening to chat messages');
        }, 14000);
      } catch (err) {
        // When this error is caught, it means the initial connection to the
        // events server failed.  No automatic attempts to reconnect will be made.
        console.error('Failed to connect to server', err);
      }
    })();
  },
  beforeDestroy() {
    // Make sure to close the connection with the events server
    // when the component is destroyed, or we'll have ghost connections!
    msgServer.close();
  },
};