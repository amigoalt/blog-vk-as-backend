# from playhouse.sqlite_ext import SqliteExtDatabase
from peewee import *


# database = SqliteExtDatabase('blog.db', pragmas={'foreign_keys': 1})
database = SqliteDatabase('blog.db', pragmas={
    'foreign_keys': 1,
    'permanent': True
    })